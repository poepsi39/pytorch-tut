Build
-----

Create and activate a new virtual environment

    virtualenv .venv
    source .venv/bin/activate

Install requirements

    pip install https://download.pytorch.org/whl/cpu/torch-1.0.1.post2-cp37-cp37m-linux_x86_64.whl
    pip install -r requirements.txt

